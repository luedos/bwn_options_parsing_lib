#include "options-parsing/optionsParser.hpp"

#include <algorithm>

void bwn::OptionsParser::parse(int p_argc, char const *const * p_argv)
{
	m_failedArgs.clear();

	StringIterator const end = p_argv + p_argc;
	StringIterator beg = findNextVar(p_argv, end);

	while(beg != end)
	{
		VariablesMap::const_iterator registrationIt = std::find_if(m_variables.begin(), m_variables.end(),
			[beg](std::pair<char const *, Variable> const& p_node)
			{ return strcmp(p_node.first, *beg) == 0; }
		);
		Variable const& registration = registrationIt->second;

		++beg;

		StringIterator const next = findNextVar(beg, end);

		if (!registration.setter(beg, next, registration.var))
		{
			m_failedArgs.push_back(registrationIt->first);
		}

		beg = next;
	}
}

void bwn::OptionsParser::clear()
{
	m_variables.clear();
	m_failedArgs.clear();
}

bool bwn::OptionsParser::setFlag(StringIterator&, StringIterator, void* p_var)
{
	*reinterpret_cast<bool*>(p_var) = true; 
	return true;
}

std::vector<char const *> const& bwn::OptionsParser::getFailedArgs() const
{
	return m_failedArgs;
}

std::string bwn::OptionsParser::formatHelp() const
{
	std::string ret = "Possible options:\n";
	for (const std::pair<const char*const, Variable>& var : m_variables)
	{
		ret += var.first;
		ret.push_back('\n');
	}

	return ret;
}

bool bwn::OptionsParser::isExist(char const * p_var) const
{
	return std::find_if(m_variables.begin(), m_variables.end(),
		[p_var](const std::pair<char const *, Variable>& p_node)
		{ return strcmp(p_node.first, p_var) == 0; }
	) != m_variables.end();
}

bwn::StringIterator bwn::OptionsParser::findNextVar(StringIterator p_beg, StringIterator p_end)
{
	while (p_beg != p_end)
	{
		if (isExist(*p_beg)) {
			break;
		}

		++p_beg;
	}

	return p_beg;
}