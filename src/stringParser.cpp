#include "options-parsing/stringParser.hpp"

#include <string.h>

bool bwn::StringParser<bool>::call(
	StringIterator& p_beg, 
	StringIterator p_end, 
	bool& p_value)
{
	if (p_beg >= p_end)
	{
		return false;
	}

	if (strcmp(*p_beg, "true") == 0)
	{
		p_value = true;
		++p_beg;
		return true;
	}

	if (strcmp(*p_beg, "false") == 0)
	{
		p_value = false;
		++p_beg;
		return true;
	}

	return false;
}
bool bwn::StringParser<char const *>::call(
	StringIterator& p_beg, 
	StringIterator p_end, 
	char const *& p_string)
{
	if (p_beg < p_end)
	{
		p_string = *p_beg;
		++p_beg;
		return true;
	}
	else
	{
		return false;
	}
}

bool bwn::StringParser<std::string>::call(
	StringIterator& p_beg, 
	StringIterator p_end, 
	std::string& p_string)
{
	if (p_beg < p_end)
	{
		p_string = *p_beg;
		++p_beg;
		return true;
	}
	else
	{
		return false;
	}
}