#pragma once

#include "stringParser.hpp"

#include <vector>
#include <string>
#include <unordered_map>

namespace bwn
{

class OptionsParser
{
	struct Variable
	{
		void* var;
		bool (*setter) (StringIterator&, StringIterator, void*);
	};

	using VariablesMap = std::unordered_map<char const *, Variable>;

	//
	// Public interface.
	//
public:
	// Name should be literal.
	template<typename VarT>
	void registerVaraible(char const * p_name, VarT& p_var)
	{
		bool (*setterFunc) (StringIterator&, StringIterator, void*) = 
			[](StringIterator& p_beg, StringIterator p_end, void* p_var)
			{ return StringParser<VarT>::call(p_beg, p_end, *reinterpret_cast<VarT*>(p_var)); };

		m_variables.emplace(
			p_name, 
			Variable { 
				&p_var, 
				setterFunc
			});
	}
	// Name should be literal.
	void registerOption(char const * p_name, bool& p_flag)
	{
		m_variables.emplace(p_name, Variable{ &p_flag, &setFlag });
	}

	void parse(int p_argc, char const *const * p_argv);
	void clear();
	std::vector<char const *> const& getFailedArgs() const;

	std::string formatHelp() const;

	//
	// Private methods.
	//
private:

	static bool setFlag(StringIterator&, StringIterator, void* p_var);
	bool isExist(char const* p_var) const;
	StringIterator findNextVar(StringIterator p_beg, StringIterator p_end);

	//
	// Private members.
	//
private:

	VariablesMap m_variables;
	std::vector<char const *> m_failedArgs;
};

}