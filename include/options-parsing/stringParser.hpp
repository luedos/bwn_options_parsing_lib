#pragma once

#include <string>
#include <vector>
#include <string.h>

namespace bwn
{

namespace detail
{

template<typename TypeT>
struct SimpleTypeInfo;

template<>
struct SimpleTypeInfo<int32_t>
{
	static constexpr char const * k_wildcardArr[] = { "0x%x", "0%o", "%d"  };
};
template<>
struct SimpleTypeInfo<uint32_t>
{
	static constexpr char const * k_wildcardArr[] = { "0x%x", "0%o", "%u" };
};

template<>
struct SimpleTypeInfo<int64_t>
{
	static constexpr char const * k_wildcardArr[] = { "0x%llx", "0%llo", "%lld"  };
};

template<>
struct SimpleTypeInfo<uint64_t>
{
	static constexpr char const * k_wildcardArr[] = { "0x%llx", "0%llo", "%llu"  };
};

template<>
struct SimpleTypeInfo<float>
{
	static constexpr char const * k_wildcardArr[] = { "%e", "%f"  };
};

template<>
struct SimpleTypeInfo<double>
{
	static constexpr char const * k_wildcardArr[] = { "%le", "%lf"  };
};

template<typename TypeT>
bool tryParseSimpleType(char const* p_string, TypeT& o_ret)
{
	for (char const *const wildcard : SimpleTypeInfo<TypeT>::k_wildcardArr)
	{
		TypeT temp;
		if (sscanf(p_string, wildcard, &temp) != -1)
		{
			o_ret = temp;
			return true;
		}
	}
	return false;
}

}

using StringIterator = char const *const *;

// We are using struct instead of simple function, because this way it's eazier to do partial specializations.
// Any implementation of this struct should have static method 'call' with signature:
// bool(StringIterator p_beg, StringIterator p_end, VarT& p_var)
// The object of this type will not be created, only it's function will be used as pointer.
// The begin iterator should be incremented to the amount of the parsed variables.
template<typename VarT, typename = void>
struct StringParser;

template<typename VarT>
struct StringParser<VarT, std::void_t<decltype(detail::SimpleTypeInfo<VarT>::k_wildcardArr)>>
{
	static bool call(
		StringIterator& p_beg, 
		StringIterator p_end, 
		VarT& p_var)
	{		
		if (p_beg >= p_end) 
		{
			return false;
		}
		const bool success = detail::tryParseSimpleType(*p_beg, p_var);
		p_beg += static_cast<int>(success);
		return success;
	}
};

template<>
struct StringParser<bool, void>
{
	static bool call(
		StringIterator& p_beg, 
		StringIterator p_end, 
		bool& p_value);
};

template<>
struct StringParser<char const *, void>
{
	static bool call(
		StringIterator& p_beg, 
		StringIterator p_end, 
		char const *& p_string);
};

template<>
struct StringParser<std::string, void>
{
	static bool call(
		StringIterator& p_beg, 
		StringIterator p_end, 
		std::string& p_string);
};

template<typename ItemT>
struct StringParser<std::vector<ItemT>, std::void_t<decltype(StringParser<ItemT>::call)>>
{
	static bool call(
		StringIterator& p_beg, 
		StringIterator p_end, 
		std::vector<ItemT>& p_vector)
	{
		StringIterator currentIt = p_beg;		
		std::vector<ItemT> tempVector;

		while(currentIt < p_end)
		{
			ItemT temp;
			if(!StringParser<ItemT>::call(currentIt, p_end, temp))
			{
				return false;
			}
			tempVector.push_back(std::move(temp));
		}

		p_vector = std::move(tempVector);
		p_beg = currentIt;

		return true;
	}
};

}